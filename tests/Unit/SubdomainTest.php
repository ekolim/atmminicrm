<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Tests\TestCase;

class SubdomainTest extends TestCase
{
    /**
     * Eko Notes:
     * For the specific registered subdomain depends on each user's local configuration.
     * I am using laravel valet even though I register specific subdomain as long as I type unregistered subdomain still return 200 status code
     * But in Browser return 404 for invalid subdomain, I think it's due my default valet configuration
     */
    
    /**
     * A basic unit test example.
     *
     * @return void
     */

    /** @test */
    public function valid_if_got_subdomain()
    {
        $this->get('http://joko.atmminicrm.test')->assertStatus(200);
    }

    /** @test */
    public function invalid_if_dont_have_subdomain()
    {
        $this->get('http://atmminicrm.test')->assertStatus(404);
    }
}
