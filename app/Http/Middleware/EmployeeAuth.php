<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class EmployeeAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('employee_auth')){
            $employee_auth = Session::get('employee_auth');
            if($employee_auth->subdomain == Route::input('subdomain')){
                return $next($request);
            }
        }

        $request->session()->invalidate();
        return redirect('/');
    }
}
