<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('employee.auth');
    }

    public function index()
    {
        return view('admin.admin');
    }

    public function logout(Request $request)
    {
        $url = 'http://127.0.0.1:8002';

        $token = Session::get('employee_auth')->jwt_token;
        
        $response_raw = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])->post($url . '/api/employee-auth/logout');

        $request->session()->invalidate();
        
        return redirect('/');
    }
}
