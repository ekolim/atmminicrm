<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('employee.auth');
    }
    
    public function index()
    {
        return view('admin.employee');
    }
}
