<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class EmployeeLoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function processLogin(Request $request)
    {
        $url = 'http://127.0.0.1:8002';
        $response_raw = Http::post($url . '/api/employee-auth/login', [
            'email' => $request->email,
            'password' => $request->password,
            'subdomain' => Route::input('subdomain')
        ]);

        $response = json_decode($response_raw->getBody()->getContents());

        if($response->authenticated){
            Session::put('employee_auth', $response);
            return redirect('/dashboard');
        }else{
            return 'Not Authenticated';
        }
    }
}
