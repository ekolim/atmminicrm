@extends('admin.admin')
@section('content')

<input type="hidden" id="company_id" value="{{ Session::get('employee_auth')->company_id }}" />
<input type="hidden" id="token" value="{{ Session::get('employee_auth')->jwt_token }}" />
<table id="table_employee" class="display">
    <thead>
        <tr>
            <th>id</th>
            <th>No</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

@endsection

@section('script1')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
    // Source https://w3path.com/laravel-6-ajax-crud-tutorial-using-datatables-from-scratch/
    $(document).ready(function(){
        let company_id = document.getElementById('company_id').value;
        let token = document.getElementById('token').value;

        $('#table_employee').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "http://127.0.0.1:8002/api/employee-auth/list-employee/" + company_id,
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", "Bearer " + token);
                },
                type: 'GET',
            },
            columns: [
                    {data: 'id', name: 'id', 'visible': false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email' },
                    {data: 'phone', name: 'phone'},
                ],
            order: [[0, 'desc']],
        });
    });    
</script>
    
@endsection
