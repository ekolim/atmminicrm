<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::domain('{subdomain}.atmminicrm.test')->group(function () {
    Route::get('/', 'EmployeeLoginController@showLoginForm');
    Route::post('/submit-login', 'EmployeeLoginController@processLogin');
    
    Route::get('/dashboard' , 'DashboardController@index');
    Route::post('/employee/logout' , 'DashboardController@logout');

    Route::get('/list-employee' , 'EmployeeController@index');
});

// Route::get('/', function () {
//    return view('welcome');
// });

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
